const Dialpad = {};

(() => {
	function sumDigitsIn(str) {
		// sum the value of the digits and ignore non digit characters
		return [...str]
			.filter(digit => '0123456789'.includes(digit))
			.reduce((currentValue, digit) => currentValue + parseInt(digit, 10), 0);
	}

	/**
	 * Binds onclick callbacks to the given dialpad buttons and forward their input to the given output field.
	 	The buttons and output field could be HTMLElements but they could also be other objects which have their
	 	onclick property called in another way. 

	 * @param {Object} dialpad - An dialpad object with the following properties:

	 * @param {Object[]} dialpad.buttons - Those buttons will have an onclick property assigned to 
	 		them that, when called, will add the button's value to the outputField's value.

	 * @param {Object} dialpad.outputField - When a button's onclick is called, the button's
	 		value-property will be appended to this object's value-property.

	 * @param {Object} dialpad.sumButton - This object will have an onclick property assigned to it
	 		that will calculate the sum of the digits 

	 * @param {function(int)} dialpad.reportSum - In the sum button's onclick this callback will
	 		be called with the sum of the digits currently in the output field.
	 */
	Dialpad.initDialpad = ({ buttons, outputField, sumButton, reportSum }) => {
		buttons.forEach(button => { button.onclick = () => { outputField.value += button.value } });
		sumButton.onclick = () => reportSum(sumDigitsIn(outputField.value));
	}
})();