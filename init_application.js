window.addEventListener("DOMContentLoaded", function() {
	const dialpad = document.getElementsByClassName('dialpad')[0];
	Dialpad.initDialpad({
		buttons: Array.from(dialpad.getElementsByClassName('dialpad-button')),
		outputField: dialpad.getElementsByClassName('dialpad-output')[0],
		sumButton: dialpad.getElementsByClassName('sum-button')[0],
		reportSum: sum => { console.log(sum) }
	});
})